import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavBar from "./components/NavBar";
import HomePage from "./components/Homepage";
import BookMarkPage from "./components/BookMarks";
import AuthPage from "./components/AuthPage";
import DetailsPage from "./components/DetailsPage";
import SearchResult from "./components/SearchResult";
import { Provider } from "react-redux";
import store from "./components/store/index";
function App() {
  return (
    <Router>
      <Provider store={store}>
        <NavBar />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/bookmarks" exact component={BookMarkPage} />
          <Route path="/auth" exact component={AuthPage} />
          <Route path="/details" exact component={DetailsPage} />
          <Route path="/home" exact component={SearchResult} />
        </Switch>
      </Provider>
    </Router>
  );
}

export default App;

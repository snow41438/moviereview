import React, { Component } from "react";
import styled from "styled-components";
import { Input, Button } from "antd";
import { Link } from "react-router-dom";

const Wrapper = styled.div``;

class Authentication extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <Link to="/auth">
          <Button type="link">Login/SignUp</Button>
        </Link>
      </Wrapper>
    );
  }
}

export default Authentication;

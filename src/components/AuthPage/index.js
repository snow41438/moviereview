import React, { Component } from "react";
import styled from "styled-components";
import { Input, Button, Form, Row, Col, Icon } from "antd";

const Wrapper = styled.div``;

const Container = styled.div`
  padding: 50px;
  text-align: center;
`;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class Authentication extends Component {
  state = {
    email: "",
    password: ""
  };
  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        alert("success");
      }
    });
  };

  render() {
    const {
      getFieldDecorator,
      getFieldError,
      isFieldTouched
    } = this.props.form;
    const usernameError =
      isFieldTouched("username") && getFieldError("username");
    const passwordError =
      isFieldTouched("password") && getFieldError("password");
    return (
      <Wrapper>
        <Container>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item
              validateStatus={usernameError ? "error" : ""}
              help={usernameError || ""}
            >
              {getFieldDecorator("username", {
                rules: [
                  { required: true, message: "Please input your username!" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  style={{ width: "50%" }}
                  placeholder="Username"
                  onChange={e => this.setState({ email: e.target.value })}
                />
              )}
            </Form.Item>

            <Form.Item
              validateStatus={passwordError ? "error" : ""}
              help={passwordError || ""}
            >
              {getFieldDecorator("password", {
                rules: [
                  { required: true, message: "Please input your password!" }
                ]
              })(
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  style={{ width: "50%" }}
                  type="password"
                  placeholder="Password"
                  onChange={e => this.setState({ password: e.target.value })}
                />
              )}
            </Form.Item>

            {/* <Form.Item> */}
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleSubmit}
              style={{ textAlign: "right", fontSize: 20 }}
            >
              Submit
            </Button>
            {/* </Form.Item> */}
          </Form>
        </Container>
      </Wrapper>
    );
  }
}
const HorizontalLoginForm = Form.create({ name: "normal_login" })(
  Authentication
);

export default HorizontalLoginForm;

import React, { Component } from "react";
import axios from "axios";
import { Spin, Icon } from "antd";
import styled from "styled-components";
import { connect } from "react-redux";
import Card from "../Homepage/HomeView/SearchCard";
const Wrapper = styled.div`
  padding: 40px 40px;
`;
const Container = styled.div``;
const antIcon = (
  <Icon
    type="loading"
    style={{ fontSize: "300px", alignSelf: "center" }}
    spin
  />
);

const Title = styled.h1`
  padding: 20px 0 0 20px;
  font-weight: 600;
  margin-bottom: 20px;
`;

class SearchResult extends Component {
  state = {
    result: []
  };

  // componentWillUpdate = (nextProps, nextState) => {
  //   console.log("will update ", this.props);
  //   // if (this.props !== nextProps) {
  //   // if (this.props.searchResult !== undefined) {
  //   //   console.log("inside ", this.props);
  //   if (this.props.searchResult) {
  //     this.setState({ result: this.props.searchResult });
  //   } // }
  // };
  render() {
    console.log("state", this.state);
    const result = (this.props.searchResult || []).map(d => <Card data={d} />);
    return (
      <Wrapper>
        <Container>
          <Title>Search Results</Title>
          {/* {this.state.result ? (
          (this.props.searchResult || []).map(d => <Card data={d} />)
          : (
          <Spin indicator={antIcon} />
          )} */}
          {result}
        </Container>
      </Wrapper>
    );
  }
}

const mapstatetoprops = state => {
  console.log("mapstatetoprops ", state);
  return {
    ...state
  };
};

export default connect(mapstatetoprops)(SearchResult);

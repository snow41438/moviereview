import ActionTypes from "./ActionTypes";
import axios from "axios";
import store from "../store/index";
const key_url =
  "https://api.themoviedb.org/3/search/movie?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&query=";
const query = "&page=1&include_adult=false";

export const searchData = data => {
  console.log("data ", data);
  axios.get(key_url + data + query).then(response => {
    console.log("response ", response.data);
    const result = response.data;
    const action = {
      type: ActionTypes.SEARCH_RESULT,
      payload: result
    };
    store.dispatch(action);
  });
};

import React, { Component } from "react";
import { Input, Button } from "antd";
import styled from "styled-components";
import { Link } from "react-router-dom";
import axios from "axios";
import { searchData } from "../action/Action";
import { connect } from "react-redux";
const key_url =
  "https://api.themoviedb.org/3/search/movie?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&query=";
const query = "&page=1&include_adult=false";
const Wrapper = styled.div``;
const Container = styled.div`
  display: flex;
  width: 50%;
`;
const HomepageHndler = styled.div`
  display: none;
`;

class SearchBar extends Component {
  state = {
    keyword: "",
    searchResult: []
  };

  searchHandler = () => {
    // axios.get(key_url + this.state.keyword + query).then(response => {
    //   console.log("response ", response.data);
    //   this.setState({ searchResult: response.data.results });
    // });
    searchData(this.state.keyword);
  };
  render() {
    const { searchResult } = this.state;
    console.log(this.props);
    return (
      <Wrapper>
        <Container>
          <Input
            placeholder="input keyword to search"
            value={this.state.keyword}
            onChange={e => this.setState({ keyword: e.target.value })}
          />
          <Link
            to={{
              pathname: "/home",
              state: {
                searchResult: searchResult
              }
            }}
          >
            <Button
              style={{ backgroundColor: "#cc7a00", color: "#fff" }}
              onClick={this.searchHandler}
            >
              Search
            </Button>
          </Link>
        </Container>
      </Wrapper>
    );
  }
}

const mapstatetoprops = state => {
  // console.log("mapstatetoprops", state);
};

export default connect(mapstatetoprops)(SearchBar);

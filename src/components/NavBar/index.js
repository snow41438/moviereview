import React, { Component } from "react";
import styled from "styled-components";
import SearchBar from "./SearchBar";
import Auth from "../AuthPage/Auth";
import { Link } from "react-router-dom";
const Wrapper = styled.div`
  background-color: #404040;
  height: 100px;
  position: sticky;
  top: 0;
  z-index:1
`;
const Container = styled.div`
  display: flex;
  /* flex-direction: column; */
`;
const SearchContainer = styled.div`
  margin: 15px 0 0 20px;
`;
const AuthContainer = styled.div`
  align-self: flex-end;
  margin-right: 15px;
`;
const LogoCOntainer = styled.div`
  flex: 1;
  /* justify-content: center;
  align-content: center; */
  padding: 15px;
`;
const SIdeCOntainer = styled.div`
  display: flex;
  flex: 3;
  flex-direction: column;
`;

class Index extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <Container>
          <LogoCOntainer>
            <Link to="/">
              <img src="https://img.icons8.com/nolan/64/000000/documentary.png" />{" "}
            </Link>
          </LogoCOntainer>
          <SIdeCOntainer>
            <AuthContainer>
              <Auth />
            </AuthContainer>
            <SearchContainer>
              <SearchBar />
            </SearchContainer>
          </SIdeCOntainer>
        </Container>
      </Wrapper>
    );
  }
}

export default Index;

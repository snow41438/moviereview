import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";

const Wrapper = styled.div``;
class DetailsPage extends Component {
  state = {};
  render() {
    return <Wrapper>Details page</Wrapper>;
  }
}

const mapStateToProps = state => {
  return { ...state };
};
export default connect(mapStateToProps)(DetailsPage);

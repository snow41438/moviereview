import React, { Component } from "react";
import styled from "styled-components";
import HomeView from "./HomeView/index";

const Wrapper = styled.div`
  width: 100%;
`;
const Container = styled.div`
  padding: 50px 50px;
  /* display: flex; */
  /* background-color: green; */
  /* display: inline-flex; */
`;
const Title = styled.div`
  font-size: 25px;
  font-weight: 600;
  margin-left: 30px;
`;
class HomePage extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <Container>
          <Title>All time popular</Title>
          <HomeView />
        </Container>
      </Wrapper>
    );
  }
}

export default HomePage;

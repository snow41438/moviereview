import React, { Component } from "react";
import axios from "axios";
import styled from "styled-components";
import StarRatings from "react-star-ratings";

const poster = "https://image.tmdb.org/t/p/original";
const Wrapper = styled.div`
  /* box-shadow: rgb(235, 237, 239) 1px 2px; */
  margin-bottom: 30px;
  margin-right: 5px;
  /* padding: 10px; */
  /* border-radius: 10px; */
  /* box-shadow: 0px 2px 4px 0px #ddd; */
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  transition: 0.3s;
  cursor: pointer;
`;
const Container = styled.div`
  display: flow-root;
  width: 100%;
  height: 100%;
  background-color: black;
  /* border-radius: 10px; */
`;
const PosterContainer = styled.div`
  width: 300px;
  height: 250px;
`;
const Poster = styled.img`
  object-fit: cover;
`;

const Overview = styled.div`
  word-wrap: break-word;
  font-size: 15px;
`;
const DetailsContainer = styled.div`
  position: relative;
`;
const Title = styled.div`
  word-wrap: break-word;
`;
const Release = styled.div``;
const Details = styled.div`
  padding: 5px 5px;
  color: #fff;
  width: 300px;
  font-size: 20px;
  font-weight: 500;
  background-color: #000;
  overflow-wrap: break-word;
`;

class HomeCard extends Component {
  state = {
    poster: ""
  };
  componentDidMount = () => {
    axios.get(poster + this.props.data.poster_path).then(response => {
      this.setState({ poster: response.config.url });
    });
  };
  render() {
    var data = this.props.data;
    // console.log(data);
    return (
      <Wrapper>
        <Container>
          <PosterContainer>
            <Poster
              src={this.state.poster}
              alt="Smiley face"
              width="100%"
              height="100%"
            />
          </PosterContainer>
          <Details>
            <Title>
              <span>
                <b>{data.title}</b>
              </span>
            </Title>
            <Release>
              Release Date <i>{data.release_date}</i>
            </Release>
            <StarRatings
              rating={5}
              starRatedColor="blue"
              numberOfStars={6}
              name="rating"
              starDimension="18px"
              style={{ zIndex: -1 }}
            />
            <Overview>{data.overview.substring(0, 100)} .....</Overview>
          </Details>
        </Container>
      </Wrapper>
    );
  }
}

export default HomeCard;

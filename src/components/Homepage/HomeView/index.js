import React, { Component } from "react";
import styled from "styled-components";
import HomeCard from "./HomeCard";
import axios from "axios";
// import { popular_movies } from "../../../Urls";

const Wrapper = styled.div`
  height: 5000px;
`;
const Container = styled.div`
  display: flex;
  flex-flow: wrap;
  width: 100%;
  justify-content: space-evenly;
`;
const url =
  "https://api.themoviedb.org/3/movie/popular?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&page=1";
const imgurl =
  "https://image.tmdb.org/t/p/original/5vTaUnfrEt6nNmB72MfNHG1p7x.jpg";

class HomeView extends Component {
  state = {
    movieData: []
  };
  componentDidMount = () => {
    axios.get(url).then(response => {
      this.setState({ movieData: response.data.results });
    });
  };

  render() {
    const View = this.state.movieData.map((d, i) => {
      console.log(i, " ", d);
      return <HomeCard data={d} />;
    });
    return (
      <Wrapper>
        <Container>{View}</Container>
      </Wrapper>
    );
  }
}

export default HomeView;

import React, { Component } from "react";
import axios from "axios";
import styled from "styled-components";
import StarRatings from "react-star-ratings";

const poster = "https://image.tmdb.org/t/p/original";
const Wrapper = styled.div`
  box-shadow: rgb(235, 237, 239) 1px 2px;
  margin-bottom: 30px;
  margin-right: 5px;
  padding: 10px;
  border-radius: 10px;
  cursor: pointer;
  /* width: fit-content; */
`;
const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  background-color: black;
  border-radius: 10px;
`;
const PosterContainer = styled.div`
  width: 300px;
  height: 250px;
  flex: 1;
`;
const Poster = styled.img`
  object-fit: cover;
`;

const Overview = styled.div`
  /* word-wrap: break-word; */
  color: #fff;
  font-size: 20px;
`;
const DetailsContainer = styled.div`
  /* position: relative; */
  flex: 3;
  margin-left: 3%;
`;
const Title = styled.h1`
  word-wrap: break-word;
  color: darkgoldenrod;
`;
const Release = styled.div`
  color: #fff;
`;
const Details = styled.div`
  padding: 5px 5px;
  /* color: #fff; */
  /* width: 300px; */
  font-size: 20px;
  font-weight: 500;
  background-color: #000;
  overflow-wrap: break-word;
`;

class HomeCard extends Component {
  state = {
    poster: ""
  };
  componentDidMount = () => {
    axios.get(poster + this.props.data.poster_path).then(response => {
      this.setState({ poster: response.config.url });
    });
  };
  render() {
    var data = this.props.data;
    console.log(data);
    return (
      <Wrapper>
        <Container>
          <PosterContainer>
            <Poster
              src={this.state.poster}
              alt="Smiley face"
              width="100%"
              height="100%"
            />
          </PosterContainer>
          <DetailsContainer>
            <Details>
              <Title>
                <span>
                  <b>{data.title}</b>
                </span>
              </Title>
              <Release>
                Release Date <i>{data.release_date}</i>
              </Release>
              <StarRatings
                rating={5}
                starRatedColor="blue"
                numberOfStars={6}
                name="rating"
                starDimension="18px"
              />
              <Overview>{data.overview.substring(0, 200)} ....</Overview>
            </Details>
          </DetailsContainer>
        </Container>
      </Wrapper>
    );
  }
}

export default HomeCard;

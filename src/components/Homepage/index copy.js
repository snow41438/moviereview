import React, { Component } from "react";
import styled from "styled-components";
import HomeView from "./HomeView/index";
import SearchBar from "../NavBar/SearchBar";
const Wrapper = styled.div`
  background-color: #404040;

  width: 100%;
  height: 100%;

  position: absolute;
`;

const Title = styled.div`
  font-size: 50px;
  font-weight: 600;
  color: #fff;
  margin-bottom: 10px;
`;
const Container = styled.div`
  left: 25%; /* uncomment if you want to center horizontally */
  top: 25%; /* uncomment if you want to center vertically */
  width: 75%;
  height: 75%;
  position: relative;
  text-align: left;
`;
const HomEThings = styled.div``;
class HomePage extends Component {
  state = {
    visible: false
  };

  searchHandler = e => {};
  render() {
    return (
      <Wrapper>
        <Container>
          <HomEThings>
            <Title>Home page</Title>
            <SearchBar search={searchHandler} />
          </HomEThings>
          {/* <HomeView /> */}
        </Container>
      </Wrapper>
    );
  }
}

export default HomePage;

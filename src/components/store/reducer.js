import initialState from "./initialState";
import ActionTypes from "../action/ActionTypes";

const reducer = (state = initialState, action) => {
  console.log("reducer running ", action, state);

  switch (action.type) {
    case ActionTypes.SEARCH_RESULT:
      return {
        ...state,
        searchResult: action.payload.results
      };
  }
  return state;
};

export default reducer;

module.exports = {
  Poster_path:
    "https://image.tmdb.org/t/p/original/5vTaUnfrEt6nNmB72MfNHG1p7x.jpg",

  Keyword_search:
    "https://api.themoviedb.org/3/search/movie?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&query=dil&page=1&include_adult=false",

  upcoming_movies:
    "https://api.themoviedb.org/3/movie/upcoming?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&page=1",

  popular_movies:
    "https://api.themoviedb.org/3/movie/popular?api_key=24ede7eb40c298ba8bd64143b0d3d5b1&language=en-US&page=1"
};
